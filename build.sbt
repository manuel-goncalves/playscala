import sbt.Keys._
import play.sbt.PlaySettings

lazy val root = (project in file("."))
  .enablePlugins(PlayService, PlayLayoutPlugin, Common)
  .enablePlugins(JavaAppPackaging)
  .enablePlugins(JavaServerAppPackaging)
  .enablePlugins(sbtdocker.DockerPlugin)
  .enablePlugins(DockerComposePlugin)
  .settings(
    name := "scalarest",
    scalaVersion := "2.13.1",
    libraryDependencies ++= Seq(
      guice,
      jdbc,
      "org.joda" % "joda-convert" % "2.2.1",
      "net.logstash.logback" % "logstash-logback-encoder" % "6.2",
      "io.lemonlabs" %% "scala-uri" % "1.5.1",
      "net.codingwell" %% "scala-guice" % "4.2.6",
      "org.scalatestplus.play" %% "scalatestplus-play" % "5.0.0" % Test,
      "org.postgresql" % "postgresql" % "9.4-1200-jdbc41",
      "mysql" % "mysql-connector-java" % "5.1.41",
      "com.typesafe.play" %% "play-slick" % "5.0.0"
    ),
    scalacOptions ++= Seq(
      "-feature",
      "-deprecation",
      "-Xfatal-warnings"
    )
  )

  dockerfile in docker := {
      val appDir: File = stage.value
      val targetDir = "/app"

      new Dockerfile {
        from(s"frolvlad/alpine-oraclejre8:latest")
        maintainer("donkijote")
        runRaw("apk --update add bash")
        entryPoint(s"$targetDir/bin/${executableScriptName.value}")
        copy(appDir, targetDir)
      }
    }
  dockerExposedPorts ++= Seq(9000, 9001)
  

lazy val gatlingVersion = "3.3.1"
lazy val gatling = (project in file("gatling"))
  .enablePlugins(GatlingPlugin)
  .settings(
    scalaVersion := "2.12.10",
    libraryDependencies ++= Seq(
      "io.gatling.highcharts" % "gatling-charts-highcharts" % gatlingVersion % Test,
      "io.gatling" % "gatling-test-framework" % gatlingVersion % Test
    )    
  )

// Documentation for this project:
//    sbt "project docs" "~ paradox"
//    open docs/target/paradox/site/index.html
lazy val docs = (project in file("docs")).enablePlugins(ParadoxPlugin).
  settings(
    scalaVersion := "2.13.1",
    paradoxProperties += ("download_url" -> "https://example.lightbend.com/v1/download/play-samples-play-scala-rest-api-example")
  )

  //dockerBaseImage := "openjdk:jre"
  //dockerExposedPorts in Docker := Seq(9000)
  //dockerChmodType := DockerChmodType.UserGroupWriteExecute
  //dockerAdditionalPermissions += (DockerChmodType.UserGroupPlusExecute, s"/opt/docker")
  /*javaOptions in Universal ++= Seq(
  // JVM memory tuning
  "-J-Xmx1024m",
  "-J-Xms128m",
  // Since play uses separate pidfile we have to provide it with a proper path
  // name of the pid file must be play.pid
  s"-Dpidfile.path=/opt/docker/${packageName.value}/run/play.pid"
)*/


// use ++= to merge a sequence with an existing sequence
/*dockerCommands ++= Seq(
  ExecCmd("RUN", "mkdir", "u+x,g+x", s"/opt/docker/${packageName.value}"),
  ExecCmd("RUN", "mkdir", "u+x,g+x", s"/opt/docker/${packageName.value}/run"),
  ExecCmd("RUN", "chown", "-R", "daemon:daemon", s"/opt/docker/${packageName.value}/")
)*/
/*lazy val dockerSettings = Seq(
  docker <<= (docker dependsOn (AssemblyKeys.assembly in core)),

  dockerfile in docker := {
    //val artifact: File = (AssemblyKeys.assemblyOutputPath in AssemblyKeys.assembly in core).value
    //val artifactTargetPath = s"/app/${artifact.name}"
    val appDir: File = stage.value
  val targetDir = "/app"
    new Dockerfile {
      from("java")
      //add(artifact, artifactTargetPath)
      runRaw("apk --update add bash")
      entryPoint(s"$targetDir/bin/${executableScriptName.value}")
      copy(appDir, targetDir)
    }
  }
)*/

/*val dockerImageTag = Option(System.getProperty("dockerTag")).getOrElse("V.1.0.1-alpha")
  imageNames in docker := Seq(
    ImageName(
      namespace = Some("regiasdasdszurecr.io"),
      repository = "deploy/generic/microservices/"+name.value,
      //tag = Some(version.value)
      tag = Some(dockerImageTag)
    )
  )*/

/*dockerfile in docker := {
  val appDir: File = stage.value
  val targetDir = "/app"

  new Dockerfile {
    from(s"frolvlad/alpine-oraclejre8:latest")
    maintainer("redbee studios")
    runRaw("apk --update add bash")
    entryPoint(s"$targetDir/bin/${executableScriptName.value}")
    copy(appDir, targetDir)
  }
}*/
