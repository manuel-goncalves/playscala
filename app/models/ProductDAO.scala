package models

//import play.api.libs.json._
import play.api.Play
import javax.inject.Inject
import play.api.db.slick.{ DatabaseConfigProvider, HasDatabaseConfigProvider }
import scala.concurrent.Future
import slick.jdbc.JdbcProfile
import slick.jdbc.PostgresProfile.api._
import scala.concurrent.ExecutionContext.Implicits.global

/*
 * Product Data Access Object
**/
class ProductDAO @Inject() (protected val dbConfigProvider: DatabaseConfigProvider) extends HasDatabaseConfigProvider[JdbcProfile] {

    val products = TableQuery[ProductTable]

    def view(id: Int): Future[Option[Product]] = {
        db.run(products.filter(_.id === id).result.headOption)
    }

    def count(): Future[Int] = {
        db.run(products.map(_.id).length.result)
    }
    
    // TODO: add filtering functionality
    def list(): Future[Seq[Product]] = {
        //val offset = limit * (page-1)

        /*for {
          totalRows <- count()
          result <- db.run(products.drop(offset).take(limit).result)
        } yield Page(result, page, limit, totalRows)*/
        db.run(products.result)
    }
    
    def insert(product: Product): Future[Product] = {
        val insertQuery = products returning products.map(_.id) into ((x, id) => x.copy(id = id))
        val action = insertQuery += product
        db.run(action);
    }
    
    // TODO:
    // - update only present field
    def update(id: Int, product: Product): Future[Boolean] = {
        val newProduct: Product = product.copy(Some(id))
        db.run(products.insertOrUpdate(newProduct)).map { affectedRows => 
            affectedRows > 0
        }
    }
    
    def delete(id: Int): Future[Boolean] = {
        db.run(products.filter(_.id === id).delete).map { affectedRows =>
            affectedRows > 0
        }
    }
}
