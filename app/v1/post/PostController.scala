package v1.post

import javax.inject.Inject

import play.api.Logger
import play.api.data.Form
import play.api.libs.json._
import play.api.mvc._
import models.{Product, ProductDAO, Page}

import scala.concurrent.{ExecutionContext, Future}

case class PostFormInput(title: String, body: String)

/**
  * Takes HTTP requests and produces JSON.
  */
class PostController @Inject()(cc: PostControllerComponents, productDao: ProductDAO)(
    implicit ec: ExecutionContext)
    extends PostBaseController(cc) {

  private val logger = Logger(getClass)

  def index() = Action.async { request =>
        productDao.list().map { data => 
            Ok(Json.toJson(data)).as("application/json")
	    }
    }

    def create = Action.async(parse.json) { request => 
        request.body.validate[Product].map { product =>
            productDao.insert(product).map {
                result => Created(Json.toJson(result)).as("application/json")
            }.recoverWith {
                case e => Future { InternalServerError("ERROR: " + e )}
            }
        }.recoverTotal {
            e => Future { BadRequest( Json.obj("status" -> "fail", "data" -> JsError.toJson(e)) ) }
        }
    }
    
    def read(id: Int) = Action.async { request =>
        productDao.view(id).map { data =>
            data match {
                case Some(product) => Ok(Json.toJson(product)).as("application/json")
                case None => UnprocessableEntity
            }
        }
    }
    
    def update(id: Int) = Action.async(parse.json) { request => 
        request.body.validate[Product].map { product =>
            productDao.update(id, product).map { result =>
                result match {
                    case true => Ok(Json.obj("status" -> "success")).as("application/json")
                    case false => UnprocessableEntity
                }
            }.recoverWith {
                case e => Future { InternalServerError("ERROR: " + e ) }
            }
        }.recoverTotal {
            e => Future { BadRequest( Json.obj("status" -> "fail", "data" -> JsError.toJson(e)) ) }
        }
    }
    
    def delete(id: Int) = Action.async { request =>
        productDao.delete(id).map { result =>
            result match {
                case true => Ok(Json.obj("status" -> "success")).as("application/json")
                case false => UnprocessableEntity
            }
        }.recoverWith {
            case e => Future { InternalServerError("ERROR: " + e ) }
        }
    }
}
